


## **## **Soal Kedua: Python Test****

**Nama : Muhamad Iqbal Maulana** <br>
**Telp : 085791340248** <br>
**Posisi : Data Engineer** <br>
**Linkedin : https://www.linkedin.com/in/miqbalm/** <br>
**Email : iqbalmaulana.im270@gmail.com**

**Soal URL :** [Klik Disini](https://gitlab.com/jdsdataengineer/jds-dataengineer-oprec-test-case/-/blob/master/README.md?utm_campaign=oprec-dec-2021&utm_medium=googleform&utm_source=oprec-dec-2021#soal-kedua-python-test)

**Code :** Python
**Tools :** Google Collab
**URL Berita** : https://bandung.bisnis.com/

**Tahapan**
**1.** **Install Library yang Dibutuhkan**: `requests`, `beautifulsoup4`
![enter image description here](https://gitlab.com/pribadi1395821/jabar-digital-service-iqbal/de_python_2/de_python_2/-/raw/main/image/soal2-1.png)
**Gambar 2.1** Install Library Python.

**Code Install Library**
  
      !pip install requests beautifulsoup4
<br>

**2.** ****Script untuk Scraping dan Analisis****
![enter image description here](https://gitlab.com/pribadi1395821/jabar-digital-service-iqbal/de_python_2/de_python_2/-/raw/main/image/soal2-2.png)
**Gambar 2.2** Scripting di Google Collab.

**Code Scraping**

    import requests
    from bs4 import BeautifulSoup
    from collections import Counter
    import re
    import json
    from datetime import datetime
        
    # URL target
    url = 'https://bandung.bisnis.com/       
    
    # Request ke halaman target
    response = requests.get(url)
    soup = BeautifulSoup(response.content,  'html.parser')
    
    # Fungsi untuk membersihkan teks dan menghitung kata
    def  clean_and_count_words(text):
	    stop_words = set([
		    'dan',  'dari',  'di',  'dengan',  'ke',  'oleh',  'pada',  'sejak',  'sampai', 'seperti',  'untuk',  'buat',  'bagi',  'akan',  'antara',  'demi',  'hingga', 'kecuali',  'tentang',  'serta',  'tanpa',  'kepada',  'daripada',  'oleh karena itu', 'beserta',  'menuju',  'menurut',  'sekitar',  'selama',  'seluruh',  'bagaikan', 'terhadap',  'melalui',  'mengenai'
	    ])
    
    words = re.findall(r'\b\w+\b', text.lower())
    words = [word for word in words if word not  in stop_words]
    common_words = Counter(words).most_common(5)
    return  [word for word, count in common_words]

    # Temukan artikel berita  
    articles = soup.find_all('div', class_='artItem')
    result = []
    for article in articles[:10]:  # Ambil 10 berita terbaru
	    try:
    title_tag = article.find('h4', class_='artTitle')
    if title_tag:
	    title = title_tag.text.strip()
	    link = article.find('a')['href']
	    full_link = link
	    #date = article.find('div', class_='artDate')
	    #date = article_soup.find('h4')('artTitle')
	    #tgl = article.find('div', class_='artDate')
    
	    # Request ke halaman berita individual untuk mengambil konten
	    article_response = requests.get(full_link)
	    article_soup = BeautifulSoup(article_response.content,  'html.parser')
		#date = article_soup.find('h4', class_='detailsAttributeDates')
    
		content_tag = article_soup.find('article', class_='detailsContent force-17 mt40')
		if content_tag:
			content = content_tag.text.strip()
			date = article_soup.find('div', class_='detailsAttributeDates')    
		    date_text = date.text.strip()  if date else  None
			common_words = clean_and_count_words(content)

		    result.append({
			    "scrape_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
			    "story_source":  "bandung_bisnis",
			    "story_release_date": date_text,
			    "story_title": title,
			    "story_url": full_link,
			    "most_common_words": common_words
		    })
		except Exception as e:
		    print(f"Error processing article: {e}")

    # Output hasil dalam format JSON
    json_result = json.dumps(result, ensure_ascii=False, indent=4)
    print(json_result)
   
    with  open('scraped_articles.json',  'w', encoding='utf-8')  as f: 
	    f.write(json_result)

<br>

**3.** ****Hasil Json 10 Berita Terbaru****
![enter image description here](https://gitlab.com/pribadi1395821/jabar-digital-service-iqbal/de_python_2/de_python_2/-/raw/main/image/soal2-3.png)
**Gambar 2.3** Hasil Json 10 Berita Terbaru

**JSON Code**

    [
    {
        "scrape_time": "2024-05-20 05:56:21",
        "story_source": "bandung_bisnis",
        "story_release_date": "Senin, 20 Mei 2024 | 12:46",
        "story_title": "Bus Pariwisata Tidak Laik Jalan di Garut Mulai Ditertibkan",
        "story_url": "https://bandung.bisnis.com/read/20240520/549/1766896/bus-pariwisata-tidak-laik-jalan-di-garut-mulai-ditertibkan",
        "most_common_words": [
            "yang",
            "bus",
            "garut",
            "tersebut",
            "subang"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:23",
        "story_source": "bandung_bisnis",
        "story_release_date": "Senin, 20 Mei 2024 | 12:18",
        "story_title": "TPPAS Nambo Beroperasi Penuh Akhir Juni 2024",
        "story_url": "https://bandung.bisnis.com/read/20240520/549/1766889/tppas-nambo-beroperasi-penuh-akhir-juni-2024",
        "most_common_words": [
            "nambo",
            "ini",
            "tppas",
            "lulut",
            "sampah"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:24",
        "story_source": "bandung_bisnis",
        "story_release_date": "Minggu, 19 Mei 2024 | 19:05",
        "story_title": "Bey Tunjuk Kasatpol PP Jadi Plh Kadisdik Jabar",
        "story_url": "https://bandung.bisnis.com/read/20240519/549/1766403/bey-tunjuk-kasatpol-pp-jadi-plh-kadisdik-jabar",
        "most_common_words": [
            "jabar",
            "menjadi",
            "plh",
            "kadisdik",
            "pj"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:25",
        "story_source": "bandung_bisnis",
        "story_release_date": "Senin, 13 Mei 2024 | 12:37",
        "story_title": "Jemaah Haji Asal Subang Awali Keberangkatan Pertama Dari Embarkasi Kertajati",
        "story_url": "https://bandung.bisnis.com/read/20240513/549/1764859/jemaah-haji-asal-subang-awali-keberangkatan-pertama-dari-embarkasi-kertajati",
        "most_common_words": [
            "jemaah",
            "kloter",
            "haji",
            "kertajati",
            "ini"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:25",
        "story_source": "bandung_bisnis",
        "story_release_date": "Jumat, 17 Mei 2024 | 21:38",
        "story_title": "Pilkada Kota Bandung: Pj Wali Kota Tegaskan Pentingnya Kejujuran, Keamanan, dan Kondusivitas",
        "story_url": "https://bandung.bisnis.com/read/20240517/549/1766460/pilkada-kota-bandung-pj-wali-kota-tegaskan-pentingnya-kejujuran-keamanan-dan-kondusivitas",
        "most_common_words": [
            "bandung",
            "kota",
            "yang",
            "ppk",
            "pemilihan"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:27",
        "story_source": "bandung_bisnis",
        "story_release_date": "Minggu, 12 Mei 2024 | 19:51",
        "story_title": "Bey Machmudin Lepas Kloter Pertama Jemaah Haji Jawa Barat",
        "story_url": "https://bandung.bisnis.com/read/20240512/549/1764710/bey-machmudin-lepas-kloter-pertama-jemaah-haji-jawa-barat",
        "most_common_words": [
            "haji",
            "jemaah",
            "bey",
            "embarkasi",
            "bekasi"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:28",
        "story_source": "bandung_bisnis",
        "story_release_date": "Jumat, 17 Mei 2024 | 19:38",
        "story_title": "Investor Minta Jaminan Keamanan dari Pemkab Cirebon",
        "story_url": "https://bandung.bisnis.com/read/20240517/550/1766421/investor-minta-jaminan-keamanan-dari-pemkab-cirebon",
        "most_common_words": [
            "cirebon",
            "investasi",
            "yang",
            "triliun",
            "kabupaten"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:28",
        "story_source": "bandung_bisnis",
        "story_release_date": "Jumat, 10 Mei 2024 | 14:41",
        "story_title": "PPDB 2024: Orang Tua Siswa Bisa Gunakan Aplikasi Sapawarga",
        "story_url": "https://bandung.bisnis.com/read/20240510/549/1764309/ppdb-2024-orang-tua-siswa-bisa-gunakan-aplikasi-sapawarga",
        "most_common_words": [
            "ppdb",
            "sapawarga",
            "ini",
            "yang",
            "fitur"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:29",
        "story_source": "bandung_bisnis",
        "story_release_date": "Jumat, 17 Mei 2024 | 18:35",
        "story_title": "Forum Satu Data Sumedang Bakal Integrasikan Perencanaan Pembangunan Daerah",
        "story_url": "https://bandung.bisnis.com/read/20240517/549/1766422/forum-satu-data-sumedang-bakal-integrasikan-perencanaan-pembangunan-daerah",
        "most_common_words": [
            "data",
            "sumedang",
            "yang",
            "satu",
            "forum"
        ]
    },
    {
        "scrape_time": "2024-05-20 05:56:29",
        "story_source": "bandung_bisnis",
        "story_release_date": "Jumat, 17 Mei 2024 | 17:41",
        "story_title": "Mangga Gedong Gincu Bakal Dikembangkan Melalui Sistem HDDAP",
        "story_url": "https://bandung.bisnis.com/read/20240517/549/1766410/mangga-gedong-gincu-bakal-dikembangkan-melalui-sistem-hddap",
        "most_common_words": [
            "sumedang",
            "gedong",
            "gincu",
            "yudia",
            "proyek"
        ]
    }
    ]

